import csv
import psycopg2

"""
Skripta bo v podatkovno bazo vstavila podatke o postajah.
"""

# da ne bomo vsakič posebej pridobivali id-ja pokrajine, regije in tipa,
# najprej ustvarimo množico vseh pokrajin, regij, tipov in naredimo slovar id-jev
# ne moremo dati vsega v isti slovar, ker je lahko ime pokrajine in regije enako (npr. Gorenjska)

print("Reading data from files ...")

pokrajineM = set()
regijeM = set()
tipiM = {'letalisce'}

with open('raw_data/postaje_podatki.csv', 'r', encoding='utf-8') as f:
    podatki = csv.reader(f)

    for row in podatki: # ta for mora biti znotraj with..., zunaj je closed
        (ime, pokrajina, regija, obcina, preb, tip) = row
        # print(ime, pokrajina, regija, obcina, preb, tip)
        # preb = None if preb == 'NULL' else int(preb.replace(".", ""))
        if regija == 'Obalno - kraška regija':
            regija = 'Obalno-kraška regija'
        pokrajineM.add(pokrajina)
        regijeM.add(regija)
        tipiM.add(tip)

geo = {} # longitude = dolzina, latitude = visina

#with open('postaje-lon-lat-vis.txt', 'r', encoding='utf-8') as f:
    #podatki = csv.reader(f)

    #i = 1 # prvo vrstico želim preskočiti
    #for row in podatki:
        #if i != 1 and len(row) == 4:
            #(ime2, lon, lat, visina) = row
            #for (sumnik, ok) in [('Č', 'C'), ('č', 'c'), ('Š', 'S'), ('š', 's'), ('Ž', 'Z'), ('ž', 'z')]:
                #ime2 = ime2.replace(sumnik, ok)
            #lon = float(lon.strip())
            #lat = float(lat.strip())
            #visina = int(visina.strip()[:-1])
            #geo[ime2] = (lat, lon, visina)
        #i += 1

slovar = {}

with open('raw_data/postaje_podatki.csv', 'r', encoding='utf-8') as f:
    podatki = csv.reader(f)

    for row in podatki: # ta for mora biti znotraj with..., zunaj je closed
        (ime, pokrajina, regija, obcina, preb, tip) = row
        # print(ime, pokrajina, regija, obcina, preb, tip)
        preb = None if preb.lower() == 'null' else int(preb.replace(".", ""))
        pokrajina = None if pokrajina.lower() == 'null' else pokrajina
        regija = None if regija.lower() == 'null' else regija
        tip = None if tip.lower() == 'null' else tip
        if regija == 'Obalno - kraška regija':
            regija = 'Obalno-kraška regija'
        slovar[ime] = (preb, pokrajina, regija, tip)

print("Done!")
print("Connecting...")

# povežem se z bazo
from auth import AUTH

try:
    conn = psycopg2.connect(**AUTH)
except psycopg2.OperationalError as e:
    print("\nI am unable to connect to the database with ATUH dict:", AUTH)
    print("Error:", e)

cursor = conn.cursor()

print("Done!")
print("Filling tables pokrajina/regija/tip ...")

# preden poberem ustrezne id-je, moram v tabele regija, pokrajina, tip vnesti
# morebitne mankajoče podatke

# najprej iz baze poberem pokrajine, regije, tipe, ki so že vneseni

cursor.execute("""SELECT ime FROM pokrajina;""")
pokrajineBaza = [x[0] for x in cursor.fetchall()]
# print(pokrajineBaza)

cursor.execute("""SELECT ime FROM regija;""")
regijeBaza = [x[0] for x in cursor.fetchall()]
# print(regijeBaza)

cursor.execute("""SELECT opis FROM tip;""")
tipiBaza = [x[0] for x in cursor.fetchall()]
# print(tipiBaza)

cursor.execute("""SELECT ime FROM postaje;""")
postajeBaza = [x[0] for x in cursor.fetchall()]
# print(postajeBaza)

# nato v tabele vnesem še mankajoče podatke

for pok in pokrajineM:
    if pok != 'NULL' and pok not in pokrajineBaza:
       cursor.execute("INSERT INTO pokrajina(ime) VALUES (%s);", [pok])

for reg in regijeM:
    if reg != 'NULL' and reg not in regijeBaza:
       cursor.execute("INSERT INTO regija(ime) VALUES (%s);", [reg])

for t in tipiM:
    if t != 'NULL' and t not in tipiBaza:
       cursor.execute("INSERT INTO tip(opis) VALUES (%s);", [t])

conn.commit()

print("Done!")
print("Filling table postaje ...")

# zdaj, ko so napolnjene tabele regija, pokrajina, tip, lahko vnesem podatke v tabelo postaje
# za vse postaje vnesem podatke o imenu, geo. širini in dolžini ter nadmorski višini (ti podatki so NOT NULL)
# če imamo še dodatne podatke z wikipedie, vnesem še te
# pazim, da vnesem le podatke o postajah, ki jih sicer v bazi še ni

tezavni = [] # seznam problematičnih postaj
niVsehPodatkov = [] # seznam postaj, kjer ni vseh podatkov

with open('raw_data/postaje-lon-lat-vis.txt', 'r', encoding='utf-8') as f:
    podatki = csv.reader(f)

    i = 1 # prvo vrstico želim preskočiti
    for row in podatki:
        if i != 1 and len(row) == 4:
            (ime2, lon, lat, visina) = row
            for (sumnik, ok) in [('Č', 'C'), ('č', 'c'), ('Š', 'S'), ('š', 's'), ('Ž', 'Z'), ('ž', 'z')]:
                ime2 = ime2.replace(sumnik, ok)
            lon = float(lon.strip())
            lat = float(lat.strip())
            visina = int(visina.strip()[:-1])
            # za letališča poskrbimo posebej
            if 'letalisce' in ime2.lower():
                slovar[ime2] = (None, None, None, 'letalisce')
            if ime2 not in postajeBaza:
                try:
                    preb, pokrajina, regija, tip = slovar[ime2]
                    cursor.execute("""INSERT INTO postaje(ime, sirina, dolzina, visina, steviloPrebivalcev, pokrajina, regija, tip)
                    VALUES (%s, %s, %s, %s, %s,
                        (SELECT id FROM pokrajina WHERE ime = %s),
                        (SELECT id FROM regija WHERE ime = %s),
                        (SELECT id FROM tip WHERE opis = %s));""", [ime2, lat, lon, visina, preb, pokrajina, regija, tip])
                    conn.commit()
                except Exception as e:
                    niVsehPodatkov.append(ime2)
                    try:
                        cursor.execute("""INSERT INTO postaje(ime, sirina, dolzina, visina) VALUES (%s, %s, %s, %s);""", [ime2, lat, lon, visina])
                        conn.commit()
                    except Exception as e2:
                        tezavni.append(ime2)
                        # print("Težave!!")
                        print(e2)
                        # conn.rollback() # tole je tu zato,
                        # da ni težav z napako: current transaction is aborted, commands ignored until end of transaction block
        i += 1

print("Done!")
print("Problematične postaje:", tezavni)
print("Postaje, kjer ni vseh podatkov:", niVsehPodatkov)

conn.commit()
cursor.close()
conn.close()
