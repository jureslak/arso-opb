import csv

seznam = []

with open('postaje-lon-lat-vis.txt', 'r', encoding='utf-8') as f:
    podatki = csv.reader(f)

    for row in podatki:
        if len(row) >1:
            #print(row)
            seznam.append(row)

seznam.sort()

with open("postaje-lon-lat-vis-sorted.txt","w",encoding = 'utf-8') as g:
    for row in seznam:
        #print(row)
        ime, lon, lat, vis = row
        niz = ime + ','+ lon + ','+ lat + ','+vis
        print(niz,file=g)
    
