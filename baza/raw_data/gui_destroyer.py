# -*- coding: utf-8 -*-
# Skripta ki klika po zaslonu in dobi podatke iz arhiva arso
# http://meteo.arso.gov.si/met/sl/app/webmet/
# Deluje samo na mojem računalniku in je napisana za Python 2 :)
# Tukaj je bolj za zanimivost.
# Podatki so potegnjeni po za vsako postajo za vsako leto, do leta 2001,
# potem pa so po 4 leta naenkrat.

import Xlib.display
import Xlib.XK
import Xlib

display = Xlib.display.Display()
window = display.get_input_focus()._data["focus"];

def move(x, y):
    Xlib.ext.xtest.fake_input(display, Xlib.X.MotionNotify, x=x, y=y)
    display.sync()

def click(x, y, but=1):
    move(x, y)
    Xlib.ext.xtest.fake_input(display, Xlib.X.ButtonPress, but)
    display.sync()
    Xlib.ext.xtest.fake_input(display, Xlib.X.ButtonRelease, but)
    display.sync()

def middleclick(x, y):
    click(x, y, 2)

def rightclick(x, y):
    click(x, y, 3)

def keypress(keyname):
    keysym = Xlib.XK.string_to_keysym(keyname)
    keycode = display.keysym_to_keycode(keysym)
    Xlib.ext.xtest.fake_input(display, Xlib.X.KeyPress, keycode)
    display.sync()
    Xlib.ext.xtest.fake_input(display, Xlib.X.KeyRelease, keycode)
    display.sync()

keyname = {'-': 'minus'}

def write(s):
    for i in s:
        keypress(keyname.get(i, i))

def delete():
    for i in range(30):
        keypress("BackSpace")
        keypress("Delete")

import time
def wait(t=0.1): time.sleep(t)

DNEVNI_PODATKI    = ( 638, 844)
POLURNI_PODATKI   = ( 638, 808)
OBDOBJE           = ( 639, 986)
FIELD_FROM        = ( 816, 990)
FIELD_TO          = ( 990, 990)
GLAVNE_POSTAJE    = ( 992, 862)
POIZVEDI          = (1224, 960)
VSE_SPREMENLJIVKE = ( 645, 905)
VSE_SPR_POLURNI   = ( 645, 860)
POSTAJE_TAB       = ( 900, 817)
BILJE_START       = ( 655, 861)
POSTAJE_TABELA_XDIFF = 270
POSTAJE_TABELA_YDIFF = 21
FOUR_ARROW_SHIFT = 227  # da pridejo na zaslon vse postaje
SHRANI_PODATKE    = ( 662, 842)
SAVE_FILE         = (1614, 481)
NAZAJ             = ( 660, 788)

datumi = ["{}-01-01 {}-01-01".format(i, i+1).split() for i in range(1967, 1960, -1)]

keypress("F12")
wait()
click(*DNEVNI_PODATKI)
wait()
click(*OBDOBJE)
wait()
click(*FIELD_FROM)
wait()
delete()
write('1993-12-20')
wait()
click(*FIELD_TO)
wait()
delete()
write('1993-12-20')
wait()
click(*GLAVNE_POSTAJE)
wait()
click(*POIZVEDI)
wait(1)
click(*VSE_SPREMENLJIVKE)
click(*VSE_SPREMENLJIVKE)
wait()
click(*NAZAJ)
wait()

for (from_date, to_date) in datumi:
    click(*DNEVNI_PODATKI)
    wait()
    click(*OBDOBJE)
    wait()
    click(*FIELD_FROM)
    wait()
    delete()
    write(from_date)
    wait()
    click(*FIELD_TO)
    wait()
    delete()
    write(to_date)
    wait()
    click(*POIZVEDI)
    wait(1)
    for x in range(3):
        for y in range(5):
            click(*POSTAJE_TAB)
            wait()
            click(BILJE_START[0] + x*POSTAJE_TABELA_XDIFF, BILJE_START[1] + y*POSTAJE_TABELA_YDIFF)
            wait(3)
            click(*SHRANI_PODATKE)
            wait(1)
            click(*SAVE_FILE)
            wait(1)
            click(*SAVE_FILE)
            wait(0.5)
            click(*POSTAJE_TAB)
            wait(1)
    click(*NAZAJ)
