<!DOCTYPE html>
<html lang="sl" dir="ltr" class="client-nojs">
<head>
<meta charset="UTF-8" />
<title>Vogel - Wikipedija, prosta enciklopedija</title>
<meta name="generator" content="MediaWiki 1.25wmf22" />
<link rel="alternate" href="android-app://org.wikipedia/http/sl.m.wikipedia.org/wiki/Vogel" />
<link rel="alternate" type="application/x-wiki" title="Uredi stran" href="/w/index.php?title=Vogel&amp;action=edit" />
<link rel="edit" title="Uredi stran" href="/w/index.php?title=Vogel&amp;action=edit" />
<link rel="apple-touch-icon" href="//bits.wikimedia.org/apple-touch/wikipedia.png" />
<link rel="shortcut icon" href="//bits.wikimedia.org/favicon/wikipedia.ico" />
<link rel="search" type="application/opensearchdescription+xml" href="/w/opensearch_desc.php" title="Wikipedija (sl)" />
<link rel="EditURI" type="application/rsd+xml" href="//sl.wikipedia.org/w/api.php?action=rsd" />
<link rel="alternate" hreflang="x-default" href="/wiki/Vogel" />
<link rel="copyright" href="//creativecommons.org/licenses/by-sa/3.0/" />
<link rel="alternate" type="application/atom+xml" title="Atom-vir strani »Wikipedija«" href="/w/index.php?title=Posebno:ZadnjeSpremembe&amp;feed=atom" />
<link rel="canonical" href="http://sl.wikipedia.org/wiki/Vogel" />
<script>var _mwq = _mwq || []; _mwq.push( function ( mw ) { mw.config.set({"wgCanonicalNamespace":"","wgCanonicalSpecialPageName":false,"wgNamespaceNumber":0,"wgPageName":"Vogel","wgTitle":"Vogel","wgCurRevisionId":4273807,"wgRevisionId":4273807,"wgArticleId":115331,"wgIsArticle":true,"wgIsRedirect":false,"wgAction":"view","wgUserName":null,"wgUserGroups":["*"],"wgCategories":["Geografske škrbine","Triglavski narodni park","Gore v Julijskih Alpah"],"wgBreakFrames":false,"wgPageContentLanguage":"sl","wgPageContentModel":"wikitext","wgSeparatorTransformTable":[",\t.",".\t,"],"wgDigitTransformTable":["",""],"wgDefaultDateFormat":"dmy full","wgMonthNames":["","januar","februar","marec","april","maj","junij","julij","avgust","september","oktober","november","december"],"wgMonthNamesShort":["","jan.","feb.","mar.","apr.","maj","jun.","jul.","avg.","sep.","okt.","nov.","dec."],"wgRelevantPageName":"Vogel","wgRelevantArticleId":115331,"wgIsProbablyEditable":true,"wgRestrictionEdit":[],"wgRestrictionMove":[],"wgWikiEditorEnabledModules":{"toolbar":true,"dialogs":true,"hidesig":true,"preview":false,"publish":false},"wgMediaViewerOnClick":true,"wgMediaViewerEnabledByDefault":true,"wgVisualEditor":{"pageLanguageCode":"sl","pageLanguageDir":"ltr"},"wgPoweredByHHVM":true,"wgULSAcceptLanguageList":["sl-si","sl","en-gb","en"],"wgULSCurrentAutonym":"slovenščina","wgBetaFeaturesFeatures":[],"wgCategoryTreePageCategoryOptions":"{\"mode\":0,\"hideprefix\":20,\"showcount\":true,\"namespaces\":false}","wgNoticeProject":"wikipedia","wgWikibaseItemId":"Q753098","wgSiteNoticeId":"2.0"}); } );</script>
<script>var _mwq = _mwq || []; _mwq.push( function ( mw ) { mw.loader.implement("user.options",function($,jQuery){mw.user.options.set({"variant":"sl"});},{},{},{});mw.loader.implement("user.tokens",function($,jQuery){mw.user.tokens.set({"editToken":"+\\","patrolToken":"+\\","watchToken":"+\\"});},{},{},{});
/* cache key: slwiki:resourceloader:filter:minify-js:7:8cfd4e3db0e866eea1792f07f45e244b */ } );</script>
<script>var _mwq = _mwq || []; _mwq.push( function ( mw ) { mw.loader.load(["mediawiki.page.startup","mediawiki.legacy.wikibits","mediawiki.legacy.ajax","ext.centralauth.centralautologin","mmv.head","ext.imageMetrics.head","ext.visualEditor.viewPageTarget.init","ext.uls.init","ext.uls.interface","ext.centralNotice.bannerController","skins.vector.js","ext.dismissableSiteNotice"]); } );</script>
<link rel="stylesheet" href="//bits.wikimedia.org/sl.wikipedia.org/load.php?debug=false&amp;lang=sl&amp;modules=ext.uls.nojs%7Cext.visualEditor.viewPageTarget.noscript%7Cext.wikihiero%2CwikimediaBadges%7Cmediawiki.legacy.commonPrint%2Cshared%7Cmediawiki.sectionAnchor%7Cmediawiki.skinning.interface%7Cmediawiki.ui.button%7Cskins.vector.styles%7Cwikibase.client.init&amp;only=styles&amp;skin=vector&amp;*" />
<meta name="ResourceLoaderDynamicStyles" content="" />
<link rel="stylesheet" href="//bits.wikimedia.org/sl.wikipedia.org/load.php?debug=false&amp;lang=sl&amp;modules=site&amp;only=styles&amp;skin=vector&amp;*" />
<style>a:lang(ar),a:lang(kk-arab),a:lang(mzn),a:lang(ps),a:lang(ur){text-decoration:none}
/* cache key: slwiki:resourceloader:filter:minify-css:7:c6f8341d5f258571daa89c807f484bf1 */</style>
<script src="//bits.wikimedia.org/sl.wikipedia.org/load.php?debug=false&amp;lang=sl&amp;modules=startup&amp;only=scripts&amp;skin=vector&amp;*"></script>
<link rel="dns-prefetch" href="//meta.wikimedia.org" />
<!--[if lt IE 7]><style type="text/css">body{behavior:url("/w/static-1.25wmf22/skins/Vector/csshover.min.htc")}</style><![endif]-->
</head>
<body class="mediawiki ltr sitedir-ltr ns-0 ns-subject page-Vogel skin-vector action-view">
		<div id="mw-page-base" class="noprint"></div>
		<div id="mw-head-base" class="noprint"></div>
		<div id="content" class="mw-body" role="main">
			<a id="top"></a>

							<div id="siteNotice"><!-- CentralNotice --><script>document.write("\u003Cdiv class=\"mw-dismissable-notice\"\u003E\u003Cdiv class=\"mw-dismissable-notice-close\"\u003E[\u003Ca href=\"#\"\u003Eskrij\u003C/a\u003E]\u003C/div\u003E\u003Cdiv class=\"mw-dismissable-notice-body\"\u003E\u003Cdiv id=\"localNotice\" lang=\"sl\" dir=\"ltr\"\u003E\u003Ctable class=\"plainlinks\" style=\"margin: 4px 10%; border-collapse: collapse; background: #f9f9f9; border: 1px solid #f4c430;\"\u003E\n\u003Ctr\u003E\n\u003Ctd style=\"border: none; padding: 2px 0px 2px 0.9em; text-align: center;\"\u003E\n  \u003Cdiv class=\"floatright\"\u003E\u003Ca href=\"/wiki/Slika:CEE_Spring_banner-600.xcf\" class=\"image\"\u003E\u003Cimg alt=\"CEE Spring banner-600.xcf\" src=\"//upload.wikimedia.org/wikipedia/commons/thumb/4/45/CEE_Spring_banner-600.xcf/120px-CEE_Spring_banner-600.xcf.png\" width=\"120\" height=\"32\" srcset=\"//upload.wikimedia.org/wikipedia/commons/thumb/4/45/CEE_Spring_banner-600.xcf/180px-CEE_Spring_banner-600.xcf.png 1.5x, //upload.wikimedia.org/wikipedia/commons/thumb/4/45/CEE_Spring_banner-600.xcf/240px-CEE_Spring_banner-600.xcf.png 2x\" data-file-width=\"600\" data-file-height=\"160\" /\u003E\u003C/a\u003E\u003C/div\u003E\u003C/td\u003E\n\u003Ctd style=\"border: none; padding: 0.25em 0.9em; width: 100%;\"\u003E V teku je spomladanska akcija pisanja člankov o \u003Ca href=\"/wiki/Srednja_Evropa\" title=\"Srednja Evropa\"\u003Esrednje-\u003C/a\u003E in \u003Ca href=\"/wiki/Vzhodna_Evropa\" title=\"Vzhodna Evropa\"\u003Evzhoevropskih\u003C/a\u003E državah - \u003Cb\u003E\u003Ca href=\"/wiki/Wikipedija:Wikimedia_CEE_Pomlad_2015\" title=\"Wikipedija:Wikimedia CEE Pomlad 2015\"\u003EWikimedia CEE Pomlad 2015\u003C/a\u003E\u003C/b\u003E. Vabljeni k vpisu in sodelovanju!\u003Cbr /\u003E\nDržavi za ta teden: \u003Ca href=\"/wiki/Slika:Flag_of_Azerbaijan.svg\" class=\"image\" title=\"Zastava Azerbajdžana\"\u003E\u003Cimg alt=\"Zastava Azerbajdžana\" src=\"//upload.wikimedia.org/wikipedia/commons/thumb/d/dd/Flag_of_Azerbaijan.svg/22px-Flag_of_Azerbaijan.svg.png\" width=\"22\" height=\"11\" class=\"thumbborder\" srcset=\"//upload.wikimedia.org/wikipedia/commons/thumb/d/dd/Flag_of_Azerbaijan.svg/33px-Flag_of_Azerbaijan.svg.png 1.5x, //upload.wikimedia.org/wikipedia/commons/thumb/d/dd/Flag_of_Azerbaijan.svg/44px-Flag_of_Azerbaijan.svg.png 2x\" data-file-width=\"1200\" data-file-height=\"600\" /\u003E\u003C/a\u003E\u0026#160;\u003Ca href=\"/wiki/Azerbajd%C5%BEan\" title=\"Azerbajdžan\"\u003EAzerbajdžan\u003C/a\u003E (\u003Ca href=\"//meta.wikimedia.org/wiki/Wikimedia_CEE_Spring_2015/Structure/Azerbaijan\" class=\"extiw\" title=\"meta:Wikimedia CEE Spring 2015/Structure/Azerbaijan\"\u003Eideje\u003C/a\u003E) in \u003Ca href=\"/wiki/Slika:Flag_of_Belarus.svg\" class=\"image\" title=\"Zastava Belorusije\"\u003E\u003Cimg alt=\"Zastava Belorusije\" src=\"//upload.wikimedia.org/wikipedia/commons/thumb/8/85/Flag_of_Belarus.svg/22px-Flag_of_Belarus.svg.png\" width=\"22\" height=\"11\" class=\"thumbborder\" srcset=\"//upload.wikimedia.org/wikipedia/commons/thumb/8/85/Flag_of_Belarus.svg/33px-Flag_of_Belarus.svg.png 1.5x, //upload.wikimedia.org/wikipedia/commons/thumb/8/85/Flag_of_Belarus.svg/44px-Flag_of_Belarus.svg.png 2x\" data-file-width=\"900\" data-file-height=\"450\" /\u003E\u003C/a\u003E\u0026#160;\u003Ca href=\"/wiki/Belorusija\" title=\"Belorusija\"\u003EBelorusija\u003C/a\u003E (\u003Ca href=\"//meta.wikimedia.org/wiki/Wikimedia_CEE_Spring_2015/Structure/Belarus\" class=\"extiw\" title=\"meta:Wikimedia CEE Spring 2015/Structure/Belarus\"\u003Eideje\u003C/a\u003E) \u003C/td\u003E\n\n\u003C/tr\u003E\n\u003C/table\u003E\n\u003C/div\u003E\u003C/div\u003E\u003C/div\u003E");</script></div>
						<div class="mw-indicators">
</div>
			<h1 id="firstHeading" class="firstHeading" lang="sl">Vogel</h1>
						<div id="bodyContent" class="mw-body-content">
									<div id="siteSub">Iz Wikipedije, proste enciklopedije</div>
								<div id="contentSub"></div>
												<div id="jump-to-nav" class="mw-jump">
					Skoči na:					<a href="#mw-head">navigacija</a>, 					<a href="#p-search">iskanje</a>
				</div>
				<div id="mw-content-text" lang="sl" dir="ltr" class="mw-content-ltr"><div class="thumb tright">
<div class="thumbinner" style="width:352px;"><a href="/wiki/Slika:Vogel2.jpg" class="image"><img alt="" src="//upload.wikimedia.org/wikipedia/commons/thumb/1/1a/Vogel2.jpg/350px-Vogel2.jpg" width="350" height="176" class="thumbimage" srcset="//upload.wikimedia.org/wikipedia/commons/thumb/1/1a/Vogel2.jpg/525px-Vogel2.jpg 1.5x, //upload.wikimedia.org/wikipedia/commons/thumb/1/1a/Vogel2.jpg/700px-Vogel2.jpg 2x" data-file-width="1101" data-file-height="554" /></a>
<div class="thumbcaption">
<div class="magnify"><a href="/wiki/Slika:Vogel2.jpg" class="internal" title="Povečaj"></a></div>
Vogel poleti</div>
</div>
</div>
<div class="dablink">Za druge pomene glej <a href="/wiki/Vogel_(razlo%C4%8Ditev)" title="Vogel (razločitev)" class="mw-disambig">Vogel (razločitev)</a>.</div>
<div class="dablink">Za skupino vrhov v Fužinskih planinah glej <a href="/wiki/Vogli" title="Vogli">Vogli</a>.</div>
<p><b>Vogel</b> je 1923 m visok hrib, ki leži na jugovzhodnem delu <a href="/wiki/Julijske_Alpe" title="Julijske Alpe">Julijskih Alp</a>. Je tudi visokogorsko <a href="/wiki/Smu%C4%8Di%C5%A1%C4%8De_Vogel" title="Smučišče Vogel">smučišče</a> v območju <a href="/wiki/Triglavski_narodni_park" title="Triglavski narodni park">Triglavskega narodnega parka</a> in priljubljena poletna izletniška točka. Gondolska žičnica pripelje obiskovalce na Rjavo skalo na nadmorski višini 1535 m, kjer je razgled na <a href="/wiki/Bohinj" title="Bohinj">Bohinjsko kotlino</a> z <a href="/wiki/Bohinjsko_jezero" title="Bohinjsko jezero">Bohinjskim jezerom</a>.</p>
<h2><span class="mw-headline" id="Galerija">Galerija</span><span class="mw-editsection"><span class="mw-editsection-bracket">[</span><a href="/w/index.php?title=Vogel&amp;veaction=edit&amp;vesection=1" title="Spremeni razdelek: Galerija" class="mw-editsection-visualeditor">uredi</a><span class="mw-editsection-divider"> | </span><a href="/w/index.php?title=Vogel&amp;action=edit&amp;section=1" title="Spremeni razdelek: Galerija">uredi kodo</a><span class="mw-editsection-bracket">]</span></span></h2>
<ul class="gallery mw-gallery-traditional">
<li class="gallerybox" style="width: 155px">
<div style="width: 155px">
<div class="thumb" style="width: 150px;">
<div style="margin:41px auto;"><a href="/wiki/Slika:Vogel1.jpg" class="image"><img alt="" src="//upload.wikimedia.org/wikipedia/commons/thumb/8/8d/Vogel1.jpg/120px-Vogel1.jpg" width="120" height="68" data-file-width="2272" data-file-height="1281" /></a></div>
</div>
<div class="gallerytext">
<p>Panorama</p>
</div>
</div>
</li>
<li class="gallerybox" style="width: 155px">
<div style="width: 155px">
<div class="thumb" style="width: 150px;">
<div style="margin:30px auto;"><a href="/wiki/Slika:Triglav-izVogla.jpg" class="image"><img alt="" src="//upload.wikimedia.org/wikipedia/commons/thumb/5/54/Triglav-izVogla.jpg/120px-Triglav-izVogla.jpg" width="120" height="90" data-file-width="1600" data-file-height="1200" /></a></div>
</div>
<div class="gallerytext">
<p>Pogled na Triglav</p>
</div>
</div>
</li>
<li class="gallerybox" style="width: 155px">
<div style="width: 155px">
<div class="thumb" style="width: 150px;">
<div style="margin:30px auto;"><a href="/wiki/Slika:Vogel4.jpg" class="image"><img alt="" src="//upload.wikimedia.org/wikipedia/commons/thumb/9/93/Vogel4.jpg/120px-Vogel4.jpg" width="120" height="90" data-file-width="2272" data-file-height="1704" /></a></div>
</div>
<div class="gallerytext">
<p>Pogled na Bohinjsko jezero</p>
</div>
</div>
</li>
<li class="gallerybox" style="width: 155px">
<div style="width: 155px">
<div class="thumb" style="width: 150px;">
<div style="margin:30px auto;"><a href="/wiki/Slika:Vogel3.JPG" class="image"><img alt="" src="//upload.wikimedia.org/wikipedia/commons/thumb/8/8c/Vogel3.JPG/120px-Vogel3.JPG" width="120" height="90" data-file-width="2272" data-file-height="1704" /></a></div>
</div>
<div class="gallerytext">
<p>Panoramska restavracija</p>
</div>
</div>
</li>
</ul>
<h2><span class="mw-headline" id="Glej_tudi">Glej tudi</span><span class="mw-editsection"><span class="mw-editsection-bracket">[</span><a href="/w/index.php?title=Vogel&amp;veaction=edit&amp;vesection=2" title="Spremeni razdelek: Glej tudi" class="mw-editsection-visualeditor">uredi</a><span class="mw-editsection-divider"> | </span><a href="/w/index.php?title=Vogel&amp;action=edit&amp;section=2" title="Spremeni razdelek: Glej tudi">uredi kodo</a><span class="mw-editsection-bracket">]</span></span></h2>
<ul>
<li><a href="/wiki/Smu%C4%8Di%C5%A1%C4%8De_Vogel" title="Smučišče Vogel">Smučišče Vogel</a></li>
</ul>
<h2><span class="mw-headline" id="Zunanje_povezave">Zunanje povezave</span><span class="mw-editsection"><span class="mw-editsection-bracket">[</span><a href="/w/index.php?title=Vogel&amp;veaction=edit&amp;vesection=3" title="Spremeni razdelek: Zunanje povezave" class="mw-editsection-visualeditor">uredi</a><span class="mw-editsection-divider"> | </span><a href="/w/index.php?title=Vogel&amp;action=edit&amp;section=3" title="Spremeni razdelek: Zunanje povezave">uredi kodo</a><span class="mw-editsection-bracket">]</span></span></h2>
<ul>
<li><a rel="nofollow" class="external free" href="http://www.vogel.si/">http://www.vogel.si/</a> Vogel SKI center</li>
<li><a rel="nofollow" class="external text" href="http://www.hribi.net/gora.asp?gorovjeid=1&amp;id=588">Vogel - Hribi.net</a></li>
</ul>
<p><br /></p>
<div class="boilerplate metadata" id="stub" style="clear: both;">
<table cellpadding="0" cellspacing="0" style="background-color: transparent; margin-left: 20px;">
<tr>
<td><a href="/wiki/Slika:Geographylogo.svg" class="image" title="Zemlja"><img alt="Zemlja" src="//upload.wikimedia.org/wikipedia/commons/thumb/e/e9/Geographylogo.svg/20px-Geographylogo.svg.png" width="20" height="20" srcset="//upload.wikimedia.org/wikipedia/commons/thumb/e/e9/Geographylogo.svg/30px-Geographylogo.svg.png 1.5x, //upload.wikimedia.org/wikipedia/commons/thumb/e/e9/Geographylogo.svg/40px-Geographylogo.svg.png 2x" data-file-width="160" data-file-height="160" /></a></td>
<td>&#160;<i>Ta članek o <b><a href="/wiki/Geografija" title="Geografija">geografiji</a></b> je <a href="/wiki/Wikipedija:Popolni_%C5%A1krbinski_%C4%8Dlanek" title="Wikipedija:Popolni škrbinski članek" class="mw-redirect">škrbina</a>. Pomagaj Wikipediji in ga <b><a class="external text" href="//sl.wikipedia.org/w/index.php?title=Vogel&amp;action=edit">razširi</a></b>.</i></td>
</tr>
</table>
</div>


<!-- 
NewPP limit report
Parsed by mw1214
CPU time usage: 0.044 seconds
Real time usage: 0.073 seconds
Preprocessor visited node count: 99/1000000
Preprocessor generated node count: 0/1500000
Post‐expand include size: 1489/2097152 bytes
Template argument size: 281/2097152 bytes
Highest expansion depth: 8/40
Expensive parser function count: 0/500
-->

<!-- 
Transclusion expansion time report (%,ms,calls,template)
100.00%   36.212      1 - -total
 19.25%    6.970      1 - Predloga:Drugipomeni2
 15.19%    5.499      1 - Predloga:Geo-stub
 13.09%    4.742      1 - Predloga:Drugipomeni4
  7.99%    2.892      1 - Predloga:Predloga_za_škrbino
  6.36%    2.304      2 - Predloga:Razpov
  5.57%    2.018      1 - Predloga:Zapomen
-->

<!-- Saved in parser cache with key slwiki:pcache:idhash:115331-0!*!0!!*!4!* and timestamp 20150303174848 and revision id 4273807
 -->
<noscript><img src="//sl.wikipedia.org/wiki/Special:CentralAutoLogin/start?type=1x1" alt="" title="" width="1" height="1" style="border: none; position: absolute;" /></noscript></div>									<div class="printfooter">
						Vzpostavljeno iz&#160;»<a dir="ltr" href="http://sl.wikipedia.org/w/index.php?title=Vogel&amp;oldid=4273807">http://sl.wikipedia.org/w/index.php?title=Vogel&amp;oldid=4273807</a>«					</div>
													<div id='catlinks' class='catlinks'><div id="mw-normal-catlinks" class="mw-normal-catlinks"><a href="/wiki/Posebno:Kategorije" title="Posebno:Kategorije">Kategorije</a>: <ul><li><a href="/wiki/Kategorija:Geografske_%C5%A1krbine" title="Kategorija:Geografske škrbine">Geografske škrbine</a></li><li><a href="/wiki/Kategorija:Triglavski_narodni_park" title="Kategorija:Triglavski narodni park">Triglavski narodni park</a></li><li><a href="/wiki/Kategorija:Gore_v_Julijskih_Alpah" title="Kategorija:Gore v Julijskih Alpah">Gore v Julijskih Alpah</a></li></ul></div></div>												<div class="visualClear"></div>
							</div>
		</div>
		<div id="mw-navigation">
			<h2>Navigacijski meni</h2>

			<div id="mw-head">
									<div id="p-personal" role="navigation" class="" aria-labelledby="p-personal-label">
						<h3 id="p-personal-label">Osebna orodja</h3>
						<ul>
							<li id="pt-createaccount"><a href="/w/index.php?title=Posebno:Prijava&amp;returnto=Vogel&amp;type=signup" title="Predlagamo vam, da ustvarite račun in se prijavite, vendar pa to ni obvezno.">Ustvari račun</a></li><li id="pt-login"><a href="/w/index.php?title=Posebno:Prijava&amp;returnto=Vogel" title="Prijava ni obvezna, vendar je zaželena [o]" accesskey="o">Prijava</a></li>						</ul>
					</div>
									<div id="left-navigation">
										<div id="p-namespaces" role="navigation" class="vectorTabs" aria-labelledby="p-namespaces-label">
						<h3 id="p-namespaces-label">Imenski prostori</h3>
						<ul>
															<li  id="ca-nstab-main" class="selected"><span><a href="/wiki/Vogel"  title="Prikaže članek [c]" accesskey="c">Stran</a></span></li>
															<li  id="ca-talk" class="new"><span><a href="/w/index.php?title=Pogovor:Vogel&amp;action=edit&amp;redlink=1"  title="Pogovor o strani [t]" accesskey="t">Pogovor</a></span></li>
													</ul>
					</div>
										<div id="p-variants" role="navigation" class="vectorMenu emptyPortlet" aria-labelledby="p-variants-label">
												<h3 id="p-variants-label"><span>Različice</span><a href="#"></a></h3>

						<div class="menu">
							<ul>
															</ul>
						</div>
					</div>
									</div>
				<div id="right-navigation">
										<div id="p-views" role="navigation" class="vectorTabs" aria-labelledby="p-views-label">
						<h3 id="p-views-label">Pogled</h3>
						<ul>
															<li id="ca-view" class="selected"><span><a href="/wiki/Vogel" >Preberi</a></span></li>
															<li id="ca-ve-edit"><span><a href="/w/index.php?title=Vogel&amp;veaction=edit"  title="Urejanje strani z VisualEditorjem [v]" accesskey="v">Uredi</a></span></li>
															<li id="ca-edit" class=" collapsible"><span><a href="/w/index.php?title=Vogel&amp;action=edit"  title="Stran lahko uredite. Preden jo shranite, uporabite gumb za predogled. [e]" accesskey="e">Uredi kodo</a></span></li>
															<li id="ca-history" class="collapsible"><span><a href="/w/index.php?title=Vogel&amp;action=history"  title="Prejšnje redakcije strani [h]" accesskey="h">Zgodovina</a></span></li>
													</ul>
					</div>
										<div id="p-cactions" role="navigation" class="vectorMenu emptyPortlet" aria-labelledby="p-cactions-label">
						<h3 id="p-cactions-label"><span>Več</span><a href="#"></a></h3>

						<div class="menu">
							<ul>
															</ul>
						</div>
					</div>
										<div id="p-search" role="search">
						<h3>
							<label for="searchInput">Iskanje</label>
						</h3>

						<form action="/w/index.php" id="searchform">
														<div id="simpleSearch">
															<input type="search" name="search" placeholder="Iskanje" title="Preiščite wiki [f]" accesskey="f" id="searchInput" /><input type="hidden" value="Posebno:Iskanje" name="title" /><input type="submit" name="fulltext" value="Iskanje" title="Najde vneseno besedilo po straneh" id="mw-searchButton" class="searchButton mw-fallbackSearchButton" /><input type="submit" name="go" value="Pojdi na" title="Pojdi na stran z natanko takim imenom, če obstaja" id="searchButton" class="searchButton" />								</div>
						</form>
					</div>
									</div>
			</div>
			<div id="mw-panel">
				<div id="p-logo" role="banner"><a class="mw-wiki-logo" href="/wiki/Glavna_stran"  title="Glavna stran"></a></div>
						<div class="portal" role="navigation" id='p-navigation' aria-labelledby='p-navigation-label'>
			<h3 id='p-navigation-label'>Navigacija</h3>

			<div class="body">
									<ul>
													<li id="n-mainpage"><a href="/wiki/Glavna_stran" title="Obiščite Glavno stran [z]" accesskey="z">Glavna stran</a></li>
													<li id="n-Dobrodo.C5.A1li"><a href="/wiki/Wikipedija:Uvod">Dobrodošli</a></li>
													<li id="n-Izbrani-.C4.8Dlanki"><a href="/wiki/Wikipedija:Izbrani_%C4%8Dlanki">Izbrani članki</a></li>
													<li id="n-randompage"><a href="/wiki/Posebno:Naklju%C4%8Dno" title="Naložite naključno stran [x]" accesskey="x">Naključni članek</a></li>
													<li id="n-recentchanges"><a href="/wiki/Posebno:ZadnjeSpremembe" title="Seznam zadnjih sprememb Wikipedije [r]" accesskey="r">Zadnje spremembe</a></li>
											</ul>
							</div>
		</div>
			<div class="portal" role="navigation" id='p-obcestvo' aria-labelledby='p-obcestvo-label'>
			<h3 id='p-obcestvo-label'>Občestvo</h3>

			<div class="body">
									<ul>
													<li id="n-portal"><a href="/wiki/Wikipedija:Portal_ob%C4%8Destva" title="O projektu, kaj lahko storite, kje lahko kaj najdete">Portal občestva</a></li>
													<li id="n-Pod-lipo"><a href="/wiki/Wikipedija:Pod_lipo">Pod lipo</a></li>
													<li id="n-contact"><a href="/wiki/Wikipedija:Stik_z_nami">Kontaktna stran</a></li>
											</ul>
							</div>
		</div>
			<div class="portal" role="navigation" id='p-podpora' aria-labelledby='p-podpora-label'>
			<h3 id='p-podpora-label'>Podpora</h3>

			<div class="body">
									<ul>
													<li id="n-help"><a href="https://www.mediawiki.org/wiki/Special:MyLanguage/Help:Contents" title="Kraj za pomoč">Pomoč</a></li>
													<li id="n-sitesupport"><a href="//donate.wikimedia.org/wiki/Special:FundraiserRedirector?utm_source=donate&amp;utm_medium=sidebar&amp;utm_campaign=C13_sl.wikipedia.org&amp;uselang=sl" title="Podprite nas">Denarni prispevki</a></li>
											</ul>
							</div>
		</div>
			<div class="portal" role="navigation" id='p-coll-print_export' aria-labelledby='p-coll-print_export-label'>
			<h3 id='p-coll-print_export-label'>Tiskanje/izvoz</h3>

			<div class="body">
									<ul>
													<li id="coll-create_a_book"><a href="/w/index.php?title=Posebno:Book&amp;bookcmd=book_creator&amp;referer=Vogel">Ustvari e-knjigo</a></li>
													<li id="coll-download-as-rdf2latex"><a href="/w/index.php?title=Posebno:Book&amp;bookcmd=render_article&amp;arttitle=Vogel&amp;oldid=4273807&amp;writer=rdf2latex">Prenesi kot PDF</a></li>
													<li id="t-print"><a href="/w/index.php?title=Vogel&amp;printable=yes" title="Natisljiva različica strani [p]" accesskey="p">Različica za tisk</a></li>
											</ul>
							</div>
		</div>
			<div class="portal" role="navigation" id='p-tb' aria-labelledby='p-tb-label'>
			<h3 id='p-tb-label'>Orodja</h3>

			<div class="body">
									<ul>
													<li id="t-whatlinkshere"><a href="/wiki/Posebno:KajSePovezujeSem/Vogel" title="Seznam vseh s trenutno povezanih strani [j]" accesskey="j">Kaj se povezuje sem</a></li>
													<li id="t-recentchangeslinked"><a href="/wiki/Posebno:RecentChangesLinked/Vogel" title="Zadnje spremembe na s trenutno povezanih straneh [k]" accesskey="k">Sorodne spremembe</a></li>
													<li id="t-specialpages"><a href="/wiki/Posebno:PosebneStrani" title="Preglejte seznam vseh posebnih strani [q]" accesskey="q">Posebne strani</a></li>
													<li id="t-permalink"><a href="/w/index.php?title=Vogel&amp;oldid=4273807" title="Stalna povezava na to različico strani">Trajna povezava</a></li>
													<li id="t-info"><a href="/w/index.php?title=Vogel&amp;action=info" title="Več informacij o strani">Podatki o strani</a></li>
													<li id="t-wikibase"><a href="//www.wikidata.org/wiki/Q753098" title="Povežite na ustrezni predmet v podatkovnem odložišču [g]" accesskey="g">Objekt Wikipodatki</a></li>
						<li id="t-cite"><a href="/w/index.php?title=Posebno:Navedi&amp;page=Vogel&amp;id=4273807" title="Informacije o tem, kako navajati to stran">Navedba članka</a></li>					</ul>
							</div>
		</div>
			<div class="portal" role="navigation" id='p-lang' aria-labelledby='p-lang-label'>
			<h3 id='p-lang-label'>V drugih jezikih</h3>

			<div class="body">
									<ul>
													<li class="interlanguage-link interwiki-de"><a href="//de.wikipedia.org/wiki/Vogel_(Berg)" title="Vogel (Berg) – nemščina" lang="de" hreflang="de">Deutsch</a></li>
													<li class="interlanguage-link interwiki-en"><a href="//en.wikipedia.org/wiki/Vogel_(mountain)" title="Vogel (mountain) – angleščina" lang="en" hreflang="en">English</a></li>
													<li class="interlanguage-link interwiki-fr"><a href="//fr.wikipedia.org/wiki/Vogel_(Slov%C3%A9nie)" title="Vogel (Slovénie) – francoščina" lang="fr" hreflang="fr">Français</a></li>
													<li class="interlanguage-link interwiki-nl"><a href="//nl.wikipedia.org/wiki/Vogel_(Sloveni%C3%AB)" title="Vogel (Slovenië) – nizozemščina" lang="nl" hreflang="nl">Nederlands</a></li>
													<li class="interlanguage-link interwiki-pl"><a href="//pl.wikipedia.org/wiki/Vogel_(Alpy_Julijskie)" title="Vogel (Alpy Julijskie) – poljščina" lang="pl" hreflang="pl">Polski</a></li>
													<li class="uls-p-lang-dummy"><a href="#"></a></li>
											</ul>
				<div class='after-portlet after-portlet-lang'><span class="wb-langlinks-edit wb-langlinks-link"><a href="//www.wikidata.org/wiki/Q753098#sitelinks-wikipedia" title="Uredi medjezikovne povezave" class="wbc-editpage">Uredi povezave</a></span></div>			</div>
		</div>
				</div>
		</div>
		<div id="footer" role="contentinfo">
							<ul id="footer-info">
											<li id="footer-info-lastmod"> Čas zadnje spremembe: 00:22, 29. oktober 2014.</li>
											<li id="footer-info-copyright">Besedilo se sme prosto uporabljati v skladu z dovoljenjem <a href="http://creativecommons.org/licenses/by-sa/3.0/">Creative Commons 
Priznanje avtorstva-Deljenje pod enakimi pogoji 3.0</a>; uveljavljajo se lahko dodatni pogoji. Za podrobnosti glej <a href="//wikimediafoundation.org/wiki/Terms_of_Use">Pogoje uporabe</a>.<br/>
Wikipedia® je tržna znamka neprofitne organizacije <a href="http://wikimediafoundation.org">Wikimedia Foundation Inc.</a></li>
									</ul>
							<ul id="footer-places">
											<li id="footer-places-privacy"><a href="//wikimediafoundation.org/wiki/Politika_zasebnosti" title="wikimedia:Politika zasebnosti">Politika zasebnosti</a></li>
											<li id="footer-places-about"><a href="/wiki/Wikipedija:O_Wikipediji" title="Wikipedija:O Wikipediji">O Wikipediji</a></li>
											<li id="footer-places-disclaimer"><a href="/wiki/Wikipedija:Splo%C5%A1no_zanikanje_odgovornosti" title="Wikipedija:Splošno zanikanje odgovornosti">Zanikanja odgovornosti</a></li>
											<li id="footer-places-developers"><a href="https://www.mediawiki.org/wiki/Special:MyLanguage/How_to_contribute">Razvijalci</a></li>
											<li id="footer-places-mobileview"><a href="//sl.m.wikipedia.org/w/index.php?title=Vogel&amp;mobileaction=toggle_view_mobile" class="noprint stopMobileRedirectToggle">Mobilni pogled</a></li>
									</ul>
										<ul id="footer-icons" class="noprint">
											<li id="footer-copyrightico">
															<a href="//wikimediafoundation.org/"><img src="//bits.wikimedia.org/images/wikimedia-button.png" srcset="//bits.wikimedia.org/images/wikimedia-button-1.5x.png 1.5x, //bits.wikimedia.org/images/wikimedia-button-2x.png 2x" width="88" height="31" alt="Wikimedia Foundation"/></a>
													</li>
											<li id="footer-poweredbyico">
															<a href="//www.mediawiki.org/"><img src="//bits.wikimedia.org/static-1.25wmf22/resources/assets/poweredby_mediawiki_88x31.png" alt="Powered by MediaWiki" srcset="//bits.wikimedia.org/static-1.25wmf22/resources/assets/poweredby_mediawiki_132x47.png 1.5x, //bits.wikimedia.org/static-1.25wmf22/resources/assets/poweredby_mediawiki_176x62.png 2x" width="88" height="31" /></a>
													</li>
									</ul>
						<div style="clear:both"></div>
		</div>
		<script>/*<![CDATA[*/window.jQuery && jQuery.ready();/*]]>*/</script><script>var _mwq = _mwq || []; _mwq.push( function ( mw ) { mw.loader.state({"ext.globalCssJs.site":"ready","ext.globalCssJs.user":"ready","site":"loading","user":"ready","user.groups":"ready"}); } );</script>
<script>var _mwq = _mwq || []; _mwq.push( function ( mw ) { mw.loader.load(["mediawiki.action.view.postEdit","mediawiki.user","mediawiki.hidpi","mediawiki.page.ready","mediawiki.searchSuggest","ext.wikiEditor.init","mmv.bootstrap.autostart","ext.imageMetrics.loader","ext.visualEditor.targetLoader","ext.eventLogging.subscriber","ext.wikimediaEvents.statsd","ext.navigationTiming","schema.UniversalLanguageSelector","ext.uls.eventlogger","ext.uls.interlanguage"],null,true); } );</script>
<script>var _mwq = _mwq || []; _mwq.push( function ( mw ) { document.write("\u003Cscript src=\"//bits.wikimedia.org/sl.wikipedia.org/load.php?debug=false\u0026amp;lang=sl\u0026amp;modules=site\u0026amp;only=scripts\u0026amp;skin=vector\u0026amp;*\"\u003E\u003C/script\u003E"); } );</script>
<script>var _mwq = _mwq || []; _mwq.push( function ( mw ) { mw.config.set({"wgBackendResponseTime":103,"wgHostname":"mw1086"}); } );</script>
	</body>
</html>
