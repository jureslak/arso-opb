<!DOCTYPE html>
<html lang="sl" dir="ltr" class="client-nojs">
<head>
<meta charset="UTF-8" />
<title>Dolenci, Šalovci - Wikipedija, prosta enciklopedija</title>
<meta name="generator" content="MediaWiki 1.25wmf22" />
<link rel="alternate" href="android-app://org.wikipedia/http/sl.m.wikipedia.org/wiki/Dolenci,_%C5%A0alovci" />
<link rel="alternate" type="application/x-wiki" title="Uredi stran" href="/w/index.php?title=Dolenci,_%C5%A0alovci&amp;action=edit" />
<link rel="edit" title="Uredi stran" href="/w/index.php?title=Dolenci,_%C5%A0alovci&amp;action=edit" />
<link rel="apple-touch-icon" href="//bits.wikimedia.org/apple-touch/wikipedia.png" />
<link rel="shortcut icon" href="//bits.wikimedia.org/favicon/wikipedia.ico" />
<link rel="search" type="application/opensearchdescription+xml" href="/w/opensearch_desc.php" title="Wikipedija (sl)" />
<link rel="EditURI" type="application/rsd+xml" href="//sl.wikipedia.org/w/api.php?action=rsd" />
<link rel="alternate" hreflang="x-default" href="/wiki/Dolenci,_%C5%A0alovci" />
<link rel="copyright" href="//creativecommons.org/licenses/by-sa/3.0/" />
<link rel="alternate" type="application/atom+xml" title="Atom-vir strani »Wikipedija«" href="/w/index.php?title=Posebno:ZadnjeSpremembe&amp;feed=atom" />
<link rel="canonical" href="http://sl.wikipedia.org/wiki/Dolenci,_%C5%A0alovci" />
<script>var _mwq = _mwq || []; _mwq.push( function ( mw ) { mw.config.set({"wgCanonicalNamespace":"","wgCanonicalSpecialPageName":false,"wgNamespaceNumber":0,"wgPageName":"Dolenci,_Šalovci","wgTitle":"Dolenci, Šalovci","wgCurRevisionId":4145535,"wgRevisionId":4145535,"wgArticleId":114629,"wgIsArticle":true,"wgIsRedirect":false,"wgAction":"view","wgUserName":null,"wgUserGroups":["*"],"wgCategories":["Članki, ki so potrebni čiščenja","Članki, ki so potrebni čiščenja brez parametra razlog","Škrbine o naseljih v Sloveniji","Naselja Občine Šalovci","Dolenci, Šalovci"],"wgBreakFrames":false,"wgPageContentLanguage":"sl","wgPageContentModel":"wikitext","wgSeparatorTransformTable":[",\t.",".\t,"],"wgDigitTransformTable":["",""],"wgDefaultDateFormat":"dmy full","wgMonthNames":["","januar","februar","marec","april","maj","junij","julij","avgust","september","oktober","november","december"],"wgMonthNamesShort":["","jan.","feb.","mar.","apr.","maj","jun.","jul.","avg.","sep.","okt.","nov.","dec."],"wgRelevantPageName":"Dolenci,_Šalovci","wgRelevantArticleId":114629,"wgIsProbablyEditable":true,"wgRestrictionEdit":[],"wgRestrictionMove":[],"wgWikiEditorEnabledModules":{"toolbar":true,"dialogs":true,"hidesig":true,"preview":false,"publish":false},"wgMediaViewerOnClick":true,"wgMediaViewerEnabledByDefault":true,"wgVisualEditor":{"pageLanguageCode":"sl","pageLanguageDir":"ltr"},"wgPoweredByHHVM":true,"wgULSAcceptLanguageList":["en-us","en"],"wgULSCurrentAutonym":"slovenščina","wgBetaFeaturesFeatures":[],"wgCategoryTreePageCategoryOptions":"{\"mode\":0,\"hideprefix\":20,\"showcount\":true,\"namespaces\":false}","wgNoticeProject":"wikipedia","wgWikibaseItemId":"Q680734","wgSiteNoticeId":"2.0"}); } );</script>
<script>var _mwq = _mwq || []; _mwq.push( function ( mw ) { mw.loader.implement("user.options",function($,jQuery){mw.user.options.set({"variant":"sl"});},{},{},{});mw.loader.implement("user.tokens",function($,jQuery){mw.user.tokens.set({"editToken":"+\\","patrolToken":"+\\","watchToken":"+\\"});},{},{},{});
/* cache key: slwiki:resourceloader:filter:minify-js:7:8cfd4e3db0e866eea1792f07f45e244b */ } );</script>
<script>var _mwq = _mwq || []; _mwq.push( function ( mw ) { mw.loader.load(["mediawiki.page.startup","mediawiki.legacy.wikibits","mediawiki.legacy.ajax","ext.centralauth.centralautologin","mmv.head","ext.imageMetrics.head","ext.visualEditor.viewPageTarget.init","ext.uls.init","ext.uls.interface","ext.centralNotice.bannerController","skins.vector.js","ext.dismissableSiteNotice"]); } );</script>
<link rel="stylesheet" href="//bits.wikimedia.org/sl.wikipedia.org/load.php?debug=false&amp;lang=sl&amp;modules=ext.uls.nojs%7Cext.visualEditor.viewPageTarget.noscript%7Cext.wikihiero%2CwikimediaBadges%7Cmediawiki.legacy.commonPrint%2Cshared%7Cmediawiki.sectionAnchor%7Cmediawiki.skinning.interface%7Cmediawiki.ui.button%7Cskins.vector.styles%7Cwikibase.client.init&amp;only=styles&amp;skin=vector&amp;*" />
<meta name="ResourceLoaderDynamicStyles" content="" />
<link rel="stylesheet" href="//bits.wikimedia.org/sl.wikipedia.org/load.php?debug=false&amp;lang=sl&amp;modules=site&amp;only=styles&amp;skin=vector&amp;*" />
<style>a:lang(ar),a:lang(kk-arab),a:lang(mzn),a:lang(ps),a:lang(ur){text-decoration:none}
/* cache key: slwiki:resourceloader:filter:minify-css:7:c6f8341d5f258571daa89c807f484bf1 */</style>
<script src="//bits.wikimedia.org/sl.wikipedia.org/load.php?debug=false&amp;lang=sl&amp;modules=startup&amp;only=scripts&amp;skin=vector&amp;*"></script>
<link rel="dns-prefetch" href="//meta.wikimedia.org" />
<!--[if lt IE 7]><style type="text/css">body{behavior:url("/w/static-1.25wmf22/skins/Vector/csshover.min.htc")}</style><![endif]-->
</head>
<body class="mediawiki ltr sitedir-ltr ns-0 ns-subject page-Dolenci_Šalovci skin-vector action-view">
		<div id="mw-page-base" class="noprint"></div>
		<div id="mw-head-base" class="noprint"></div>
		<div id="content" class="mw-body" role="main">
			<a id="top"></a>

							<div id="siteNotice"><!-- CentralNotice --><script>document.write("\u003Cdiv class=\"mw-dismissable-notice\"\u003E\u003Cdiv class=\"mw-dismissable-notice-close\"\u003E[\u003Ca href=\"#\"\u003Eskrij\u003C/a\u003E]\u003C/div\u003E\u003Cdiv class=\"mw-dismissable-notice-body\"\u003E\u003Cdiv id=\"localNotice\" lang=\"sl\" dir=\"ltr\"\u003E\u003Ctable class=\"plainlinks\" style=\"margin: 4px 10%; border-collapse: collapse; background: #f9f9f9; border: 1px solid #f4c430;\"\u003E\n\u003Ctr\u003E\n\u003Ctd style=\"border: none; padding: 2px 0px 2px 0.9em; text-align: center;\"\u003E\n  \u003Cdiv class=\"floatright\"\u003E\u003Ca href=\"/wiki/Slika:CEE_Spring_banner-600.xcf\" class=\"image\"\u003E\u003Cimg alt=\"CEE Spring banner-600.xcf\" src=\"//upload.wikimedia.org/wikipedia/commons/thumb/4/45/CEE_Spring_banner-600.xcf/120px-CEE_Spring_banner-600.xcf.png\" width=\"120\" height=\"32\" srcset=\"//upload.wikimedia.org/wikipedia/commons/thumb/4/45/CEE_Spring_banner-600.xcf/180px-CEE_Spring_banner-600.xcf.png 1.5x, //upload.wikimedia.org/wikipedia/commons/thumb/4/45/CEE_Spring_banner-600.xcf/240px-CEE_Spring_banner-600.xcf.png 2x\" data-file-width=\"600\" data-file-height=\"160\" /\u003E\u003C/a\u003E\u003C/div\u003E\u003C/td\u003E\n\u003Ctd style=\"border: none; padding: 0.25em 0.9em; width: 100%;\"\u003E V teku je spomladanska akcija pisanja člankov o \u003Ca href=\"/wiki/Srednja_Evropa\" title=\"Srednja Evropa\"\u003Esrednje-\u003C/a\u003E in \u003Ca href=\"/wiki/Vzhodna_Evropa\" title=\"Vzhodna Evropa\"\u003Evzhoevropskih\u003C/a\u003E državah - \u003Cb\u003E\u003Ca href=\"/wiki/Wikipedija:Wikimedia_CEE_Pomlad_2015\" title=\"Wikipedija:Wikimedia CEE Pomlad 2015\"\u003EWikimedia CEE Pomlad 2015\u003C/a\u003E\u003C/b\u003E. Vabljeni k vpisu in sodelovanju!\u003Cbr/\u003E\nDržavi za ta teden: \u003Ca href=\"/wiki/Slika:Flag_of_Azerbaijan.svg\" class=\"image\" title=\"Zastava Azerbajdžana\"\u003E\u003Cimg alt=\"Zastava Azerbajdžana\" src=\"//upload.wikimedia.org/wikipedia/commons/thumb/d/dd/Flag_of_Azerbaijan.svg/22px-Flag_of_Azerbaijan.svg.png\" width=\"22\" height=\"11\" class=\"thumbborder\" srcset=\"//upload.wikimedia.org/wikipedia/commons/thumb/d/dd/Flag_of_Azerbaijan.svg/33px-Flag_of_Azerbaijan.svg.png 1.5x, //upload.wikimedia.org/wikipedia/commons/thumb/d/dd/Flag_of_Azerbaijan.svg/44px-Flag_of_Azerbaijan.svg.png 2x\" data-file-width=\"1200\" data-file-height=\"600\" /\u003E\u003C/a\u003E\u0026#160;\u003Ca href=\"/wiki/Azerbajd%C5%BEan\" title=\"Azerbajdžan\"\u003EAzerbajdžan\u003C/a\u003E (\u003Ca href=\"//meta.wikimedia.org/wiki/Wikimedia_CEE_Spring_2015/Structure/Azerbaijan\" class=\"extiw\" title=\"meta:Wikimedia CEE Spring 2015/Structure/Azerbaijan\"\u003Eideje\u003C/a\u003E) in \u003Ca href=\"/wiki/Slika:Flag_of_Belarus.svg\" class=\"image\" title=\"Zastava Belorusije\"\u003E\u003Cimg alt=\"Zastava Belorusije\" src=\"//upload.wikimedia.org/wikipedia/commons/thumb/8/85/Flag_of_Belarus.svg/22px-Flag_of_Belarus.svg.png\" width=\"22\" height=\"11\" class=\"thumbborder\" srcset=\"//upload.wikimedia.org/wikipedia/commons/thumb/8/85/Flag_of_Belarus.svg/33px-Flag_of_Belarus.svg.png 1.5x, //upload.wikimedia.org/wikipedia/commons/thumb/8/85/Flag_of_Belarus.svg/44px-Flag_of_Belarus.svg.png 2x\" data-file-width=\"900\" data-file-height=\"450\" /\u003E\u003C/a\u003E\u0026#160;\u003Ca href=\"/wiki/Belorusija\" title=\"Belorusija\"\u003EBelorusija\u003C/a\u003E (\u003Ca href=\"//meta.wikimedia.org/wiki/Wikimedia_CEE_Spring_2015/Structure/Belarus\" class=\"extiw\" title=\"meta:Wikimedia CEE Spring 2015/Structure/Belarus\"\u003Eideje\u003C/a\u003E) \u003C/td\u003E\n\n\u003C/tr\u003E\n\u003C/table\u003E\n\u003C/div\u003E\u003C/div\u003E\u003C/div\u003E");</script></div>
						<div class="mw-indicators">
</div>
			<h1 id="firstHeading" class="firstHeading" lang="sl">Dolenci, Šalovci</h1>
						<div id="bodyContent" class="mw-body-content">
									<div id="siteSub">Iz Wikipedije, proste enciklopedije</div>
								<div id="contentSub"></div>
												<div id="jump-to-nav" class="mw-jump">
					Skoči na:					<a href="#mw-head">navigacija</a>, 					<a href="#p-search">iskanje</a>
				</div>
				<div id="mw-content-text" lang="sl" dir="ltr" class="mw-content-ltr"><table class="infobox geography vcard" style="width:23em; text-align:left">
<tr>
<th colspan="2" style="width:100%; text-align:center; background-color:#B0C4DE; font-size:1.25em; white-space:nowrap"><span class="fn org">Dolenci, Šalovci</span></th>
</tr>
<tr>
<td colspan="2" style="text-align:center; padding:0.7em 0.8em">
<div class="floatnone"><a href="/wiki/Slika:Cerkev_sv._Nikolaja_v_Dolencih.JPG" class="image"><img alt="" src="//upload.wikimedia.org/wikipedia/commons/thumb/3/33/Cerkev_sv._Nikolaja_v_Dolencih.JPG/260px-Cerkev_sv._Nikolaja_v_Dolencih.JPG" width="260" height="173" srcset="//upload.wikimedia.org/wikipedia/commons/thumb/3/33/Cerkev_sv._Nikolaja_v_Dolencih.JPG/390px-Cerkev_sv._Nikolaja_v_Dolencih.JPG 1.5x, //upload.wikimedia.org/wikipedia/commons/thumb/3/33/Cerkev_sv._Nikolaja_v_Dolencih.JPG/520px-Cerkev_sv._Nikolaja_v_Dolencih.JPG 2x" data-file-width="1600" data-file-height="1067" /></a></div>
</td>
</tr>
<tr class="mergedrow">
<td colspan="2" style="text-align:center" align="center">
<center>
<div style="width:260px;float:none;clear:none">
<div style="width:260px;padding:0">
<div style="position:relative;width:260px"><a href="/wiki/Slika:Slovenia_location_map.svg" class="image" title="Dolenci, Šalovci is located in Slovenija"><img alt="Dolenci, Šalovci is located in Slovenija" src="//upload.wikimedia.org/wikipedia/commons/thumb/e/e5/Slovenia_location_map.svg/260px-Slovenia_location_map.svg.png" width="260" height="192" srcset="//upload.wikimedia.org/wikipedia/commons/thumb/e/e5/Slovenia_location_map.svg/390px-Slovenia_location_map.svg.png 1.5x, //upload.wikimedia.org/wikipedia/commons/thumb/e/e5/Slovenia_location_map.svg/520px-Slovenia_location_map.svg.png 2x" data-file-width="1159" data-file-height="857" /></a>
<div style="position:absolute;top:13.1%;left:85.8%;height:0;width:0;margin:0;padding:0">
<div style="position:relative;text-align:center;left:-3px;top:-3px;width:6px;font-size:6px;line-height:0"><img alt="Dolenci, Šalovci" src="//upload.wikimedia.org/wikipedia/commons/thumb/0/0c/Red_pog.svg/6px-Red_pog.svg.png" title="Dolenci, Šalovci" width="6" height="6" srcset="//upload.wikimedia.org/wikipedia/commons/thumb/0/0c/Red_pog.svg/9px-Red_pog.svg.png 1.5x, //upload.wikimedia.org/wikipedia/commons/thumb/0/0c/Red_pog.svg/12px-Red_pog.svg.png 2x" data-file-width="64" data-file-height="64" /></div>
<div style="font-size:90%;line-height:110%;position:relative;top:-1.5em;width:6em;left:0.5em;text-align:left"><span style="padding:1px">Dolenci, Šalovci</span></div>
</div>
</div>
<div style="font-size:90%;padding-top:3px"></div>
</div>
</div>
<small>Geografska lega v Sloveniji</small></center>
</td>
</tr>
<tr class="mergedbottomrow">
<td colspan="2" style="text-align:center; padding-bottom:0.7em"><a href="/wiki/Geografski_koordinatni_sistem" title="Geografski koordinatni sistem">Koordinati</a>: <span style="white-space: nowrap;"><span class="plainlinks nourlexpansion"><a class="external text" href="https://tools.wmflabs.org/geohack/geohack.php?pagename=Dolenci,_%C5%A0alovci&amp;params=46_51_4.43_N_16_17_15.45_E_region:SI_type:adm1st"><span class="geo-default"><span class="geo-dms" title="Zemljevidi, zračni posnetki in drugi podatki za to lokacijo"><span class="latitude">46°51′4.43″N</span> <span class="longitude">16°17′15.45″E</span></span></span><span class="geo-multi-punct">﻿ / ﻿</span><span class="geo-nondefault"><span class="geo-dec" title="Zemljevidi, zračni posnetki in drugi podatki za to lokacijo">46.8512306°N 16.287625°E</span><span style="display:none">﻿ / <span class="geo">46.8512306; 16.287625</span></span></span></a></span><span style="font-size: small;"><span id="coordinates"><a href="/wiki/Geografski_koordinatni_sistem" title="Geografski koordinatni sistem">Koordinati</a>: <span class="plainlinks nourlexpansion"><a class="external text" href="https://tools.wmflabs.org/geohack/geohack.php?pagename=Dolenci,_%C5%A0alovci&amp;params=46_51_4.43_N_16_17_15.45_E_region:SI_type:adm1st"><span class="geo-default"><span class="geo-dms" title="Zemljevidi, zračni posnetki in drugi podatki za to lokacijo"><span class="latitude">46°51′4.43″N</span> <span class="longitude">16°17′15.45″E</span></span></span><span class="geo-multi-punct">﻿ / ﻿</span><span class="geo-nondefault"><span class="geo-dec" title="Zemljevidi, zračni posnetki in drugi podatki za to lokacijo">46.8512306°N 16.287625°E</span><span style="display:none">﻿ / <span class="geo">46.8512306; 16.287625</span></span></span></a></span></span></span></span></td>
</tr>
<tr class="mergedtoprow">
<td><b><a href="/wiki/Seznam_suverenih_dr%C5%BEav" title="Seznam suverenih držav">Država</a></b></td>
<td><a href="/wiki/Slika:Flag_of_Slovenia.svg" class="image" title="Zastava Slovenije"><img alt="Zastava Slovenije" src="//upload.wikimedia.org/wikipedia/commons/thumb/f/f0/Flag_of_Slovenia.svg/22px-Flag_of_Slovenia.svg.png" width="22" height="11" class="thumbborder" srcset="//upload.wikimedia.org/wikipedia/commons/thumb/f/f0/Flag_of_Slovenia.svg/33px-Flag_of_Slovenia.svg.png 1.5x, //upload.wikimedia.org/wikipedia/commons/thumb/f/f0/Flag_of_Slovenia.svg/44px-Flag_of_Slovenia.svg.png 2x" data-file-width="1200" data-file-height="600" /></a> <a href="/wiki/Slovenija" title="Slovenija">Slovenija</a></td>
</tr>
<tr class="mergedrow">
<td><b><a href="/wiki/Statisti%C4%8Dne_regije_Slovenije" title="Statistične regije Slovenije">Statistična regija</a></b></td>
<td><a href="/wiki/Pomurska_regija" title="Pomurska regija">Pomurska regija</a></td>
</tr>
<tr class="mergedrow">
<td><b><a href="/wiki/Pokrajine_v_Sloveniji" title="Pokrajine v Sloveniji">Tradicionalna pokrajina</a></b></td>
<td><a href="/wiki/Prekmurje" title="Prekmurje">Prekmurje</a></td>
</tr>
<tr class="mergedrow">
<td><b><a href="/wiki/Ob%C4%8Dina" title="Občina">Občina</a></b></td>
<td><a href="/wiki/Ob%C4%8Dina_%C5%A0alovci" title="Občina Šalovci">Šalovci</a></td>
</tr>
<tr class="mergedtoprow">
<td><b>Nadmorska višina</b></td>
<td>302,1&#160;m</td>
</tr>
<tr class="mergedtoprow">
<td colspan="2"><b>Prebivalstvo</b></td>
</tr>
<tr class="mergedrow">
<td>&#160;•&#160;<b>Skupno</b></td>
<td>206</td>
</tr>
<tr class="mergedtoprow">
<td><b><a href="/wiki/%C4%8Casovni_pas" title="Časovni pas">Časovni pas</a></b></td>
<td><a href="/wiki/Srednjeevropski_%C4%8Das" title="Srednjeevropski čas">CET</a> (<a href="/wiki/UTC%2B1" title="UTC+1">UTC+1</a>)</td>
</tr>
<tr class="mergedrow">
<td style="white-space:nowrap">&#160;•&#160;<b>Poletje&#160;(<a href="/wiki/Poletni_%C4%8Das" title="Poletni čas">DST</a>)</b></td>
<td><a href="/wiki/Srednjeevropski_poletni_%C4%8Das" title="Srednjeevropski poletni čas">CEST</a> (<a href="/wiki/UTC%2B2" title="UTC+2">UTC+2</a>)</td>
</tr>
<tr class="mergedtoprow">
<td><b><a href="/wiki/Seznam_po%C5%A1tnih_%C5%A1tevilk_v_Sloveniji" title="Seznam poštnih številk v Sloveniji">Poštna številka</a></b></td>
<td class="adr"><span class="postal-code">9204 <a href="/wiki/%C5%A0alovci" title="Šalovci">Šalovci</a></span></td>
</tr>
<tr class="mergedtoprow">
<td><b>Zemljevidi</b></td>
<td><a rel="nofollow" class="external text" href="http://zemljevid.najdi.si/search_maps.jsp?q=Dolenci&amp;tab=maps">Najdi.si</a>, <a rel="nofollow" class="external text" href="http://www.geopedia.si/#L410_F10115736_T13_b4_x598091.5935_y191135.046_s14">Geopedia.si</a></td>
</tr>
<tr class="mergedtoprow">
<td colspan="2" style="text-align:left; font-size:smaller"><small>Vir: <a href="/wiki/Statisti%C4%8Dni_urad_Republike_Slovenije" title="Statistični urad Republike Slovenije">SURS</a>, <a href="/wiki/Geodetska_uprava_Republike_Slovenije" title="Geodetska uprava Republike Slovenije">GURS</a>, <a href="/wiki/Popis_prebivalstva" title="Popis prebivalstva">popis prebivalstva</a> 2002 (kjer ni drugače navedeno).</small><br /></td>
</tr>
</table>
<p><b>Dolenci</b> (<a href="/wiki/Mad%C5%BEar%C5%A1%C4%8Dina" title="Madžarščina">madžarski</a> <i>Dolány</i>) so <a href="/wiki/Naselje" title="Naselje">naselje</a> v <a href="/wiki/Ob%C4%8Dina_%C5%A0alovci" title="Občina Šalovci">Občini Šalovci</a>.</p>
<table class="metadata plainlinks ambox ambox-style ambox-Cleanup" style="">
<tr>
<td class="mbox-image">
<div style="width: 52px;"><img alt="" src="//upload.wikimedia.org/wikipedia/commons/thumb/f/f2/Edit-clear.svg/40px-Edit-clear.svg.png" width="40" height="40" srcset="//upload.wikimedia.org/wikipedia/commons/thumb/f/f2/Edit-clear.svg/60px-Edit-clear.svg.png 1.5x, //upload.wikimedia.org/wikipedia/commons/thumb/f/f2/Edit-clear.svg/80px-Edit-clear.svg.png 2x" data-file-width="48" data-file-height="48" /></div>
</td>
<td class="mbox-text" style=""><span class="mbox-text-span">Ta članek <b>potrebuje čiščenje. Pri urejanju upoštevaj pravila <a href="/wiki/Wikipedija:Slogovni_priro%C4%8Dnik" title="Wikipedija:Slogovni priročnik">slogovnega priročnika</a></b>. <span class="hide-when-compact"><a href="/wiki/Predloga:Popravi/dok" title="Predloga:Popravi/dok">Razlog za čiščenje</a> ni podan. Prosimo, da <a class="external text" href="//sl.wikipedia.org/w/index.php?title=Dolenci,_%C5%A0alovci&amp;action=edit">nam pomagaš izboljšati ta članek</a>.</span> </span></td>
</tr>
</table>
<h2><a href="#Prireditve" class="mw-headline-anchor" aria-hidden="true" title="Povezava na razdelek">§</a><span class="mw-headline" id="Prireditve">Prireditve</span><span class="mw-editsection"><span class="mw-editsection-bracket">[</span><a href="/w/index.php?title=Dolenci,_%C5%A0alovci&amp;veaction=edit&amp;vesection=1" title="Spremeni razdelek: Prireditve" class="mw-editsection-visualeditor">uredi</a><span class="mw-editsection-divider"> | </span><a href="/w/index.php?title=Dolenci,_%C5%A0alovci&amp;action=edit&amp;section=1" title="Spremeni razdelek: Prireditve">uredi kodo</a><span class="mw-editsection-bracket">]</span></span></h2>
<ul>
<li>Leta <a href="/wiki/1929" title="1929">1929</a> <a href="/wiki/Borovo_gost%C3%BCvanje" title="Borovo gostüvanje">Borovo gostüvanje</a>.</li>
</ul>
<ul>
<li>Vsekakor si preberite tudi <a href="/wiki/Uporabnik:Lanfra/Borovo_gost%C3%BCvanje" title="Uporabnik:Lanfra/Borovo gostüvanje"><b>tole</b></a>.</li>
</ul>
<h2><a href="#Glej_tudi" class="mw-headline-anchor" aria-hidden="true" title="Povezava na razdelek">§</a><span class="mw-headline" id="Glej_tudi">Glej tudi</span><span class="mw-editsection"><span class="mw-editsection-bracket">[</span><a href="/w/index.php?title=Dolenci,_%C5%A0alovci&amp;veaction=edit&amp;vesection=2" title="Spremeni razdelek: Glej tudi" class="mw-editsection-visualeditor">uredi</a><span class="mw-editsection-divider"> | </span><a href="/w/index.php?title=Dolenci,_%C5%A0alovci&amp;action=edit&amp;section=2" title="Spremeni razdelek: Glej tudi">uredi kodo</a><span class="mw-editsection-bracket">]</span></span></h2>
<ul>
<li><a rel="nofollow" class="external text" href="http://www.sddolenci.net/">ŠD Dolenci</a></li>
<li><a href="/wiki/Seznam_naselij_v_Sloveniji" title="Seznam naselij v Sloveniji">Seznam naselij v Sloveniji</a></li>
<li><a href="/wiki/Fran_%C5%A0b%C3%BCl" title="Fran Šbül" class="mw-redirect">Fran Šbül</a></li>
<li><a href="/wiki/Jo%C5%BEef_Klekl_(duhovnik)" title="Jožef Klekl (duhovnik)">Jožef Klekl (duhovnik)</a></li>
<li><a href="/wiki/Feri_Lain%C5%A1%C4%8Dek" title="Feri Lainšček">Feri Lainšček</a></li>
<li><a href="/wiki/Mi%C5%A1ka_Madjari%C4%8D" title="Miška Madjarič">Miška Madjarič</a></li>
<li><a href="/wiki/Jo%C5%BEef_Konkoli%C4%8D" title="Jožef Konkolič">Jožef Konkolič</a></li>
</ul>
<p><br /></p>
<div class="boilerplate metadata" id="stub" style="clear: both;">
<table cellpadding="0" cellspacing="0" style="background-color: transparent; margin-left: 20px;">
<tr>
<td><a href="/wiki/Slika:Flag-map_of_Slovenia.svg" class="image" title="Slovenija"><img alt="Slovenija" src="//upload.wikimedia.org/wikipedia/commons/thumb/f/ff/Flag-map_of_Slovenia.svg/35px-Flag-map_of_Slovenia.svg.png" width="35" height="21" srcset="//upload.wikimedia.org/wikipedia/commons/thumb/f/ff/Flag-map_of_Slovenia.svg/53px-Flag-map_of_Slovenia.svg.png 1.5x, //upload.wikimedia.org/wikipedia/commons/thumb/f/ff/Flag-map_of_Slovenia.svg/70px-Flag-map_of_Slovenia.svg.png 2x" data-file-width="620" data-file-height="374" /></a></td>
<td>&#160;<i>Ta članek o <b><a href="/wiki/Naselje" title="Naselje">naselju</a> v <a href="/wiki/Slovenija" title="Slovenija">Sloveniji</a></b> je <a href="/wiki/Wikipedija:Popolni_%C5%A1krbinski_%C4%8Dlanek" title="Wikipedija:Popolni škrbinski članek" class="mw-redirect">škrbina</a>. Pomagaj Wikipediji in ga <b><a class="external text" href="//sl.wikipedia.org/w/index.php?title=Dolenci,_%C5%A0alovci&amp;action=edit">razširi</a></b>.</i></td>
</tr>
</table>
</div>


<!-- 
NewPP limit report
Parsed by mw1172
CPU time usage: 0.210 seconds
Real time usage: 0.307 seconds
Preprocessor visited node count: 2303/1000000
Preprocessor generated node count: 0/1500000
Post‐expand include size: 32574/2097152 bytes
Template argument size: 7455/2097152 bytes
Highest expansion depth: 22/40
Expensive parser function count: 1/500
Lua time usage: 0.009/10.000 seconds
Lua memory usage: 587 KB/50 MB
-->

<!-- 
Transclusion expansion time report (%,ms,calls,template)
100.00%  234.303      1 - -total
 71.09%  166.557      1 - Predloga:Infopolje_Naselje_v_Sloveniji
 67.39%  157.889      1 - Predloga:Infopolje_Naselje
 26.90%   63.036      1 - Predloga:Slog
 22.83%   53.493      1 - Predloga:Main_other
 21.63%   50.669      1 - Predloga:Polje_za_članke
 19.70%   46.160      1 - Predloga:Geobox_coor
 16.96%   39.727      1 - Predloga:Coord
 16.51%   38.694      1 - Predloga:Location_map+
 13.64%   31.968      1 - Predloga:Koord/prikaži/inline,title
-->

<!-- Saved in parser cache with key slwiki:pcache:idhash:114629-0!*!0!!*!4!* and timestamp 20150311184008 and revision id 4145535
 -->
<noscript><img src="//sl.wikipedia.org/wiki/Special:CentralAutoLogin/start?type=1x1" alt="" title="" width="1" height="1" style="border: none; position: absolute;" /></noscript></div>									<div class="printfooter">
						Vzpostavljeno iz&#160;»<a dir="ltr" href="http://sl.wikipedia.org/w/index.php?title=Dolenci,_Šalovci&amp;oldid=4145535">http://sl.wikipedia.org/w/index.php?title=Dolenci,_Šalovci&amp;oldid=4145535</a>«					</div>
													<div id='catlinks' class='catlinks'><div id="mw-normal-catlinks" class="mw-normal-catlinks"><a href="/wiki/Posebno:Kategorije" title="Posebno:Kategorije">Kategoriji</a>: <ul><li><a href="/wiki/Kategorija:Naselja_Ob%C4%8Dine_%C5%A0alovci" title="Kategorija:Naselja Občine Šalovci">Naselja Občine Šalovci</a></li><li><a href="/wiki/Kategorija:Dolenci,_%C5%A0alovci" title="Kategorija:Dolenci, Šalovci">Dolenci, Šalovci</a></li></ul></div><div id="mw-hidden-catlinks" class="mw-hidden-catlinks mw-hidden-cats-hidden">Skrite kategorije: <ul><li><a href="/wiki/Kategorija:%C4%8Clanki,_ki_so_potrebni_%C4%8Di%C5%A1%C4%8Denja" title="Kategorija:Članki, ki so potrebni čiščenja">Članki, ki so potrebni čiščenja</a></li><li><a href="/wiki/Kategorija:%C4%8Clanki,_ki_so_potrebni_%C4%8Di%C5%A1%C4%8Denja_brez_parametra_razlog" title="Kategorija:Članki, ki so potrebni čiščenja brez parametra razlog">Članki, ki so potrebni čiščenja brez parametra razlog</a></li><li><a href="/wiki/Kategorija:%C5%A0krbine_o_naseljih_v_Sloveniji" title="Kategorija:Škrbine o naseljih v Sloveniji">Škrbine o naseljih v Sloveniji</a></li></ul></div></div>												<div class="visualClear"></div>
							</div>
		</div>
		<div id="mw-navigation">
			<h2>Navigacijski meni</h2>

			<div id="mw-head">
									<div id="p-personal" role="navigation" class="" aria-labelledby="p-personal-label">
						<h3 id="p-personal-label">Osebna orodja</h3>
						<ul>
							<li id="pt-createaccount"><a href="/w/index.php?title=Posebno:Prijava&amp;returnto=Dolenci%2C+%C5%A0alovci&amp;type=signup" title="Predlagamo vam, da ustvarite račun in se prijavite, vendar pa to ni obvezno.">Ustvari račun</a></li><li id="pt-login"><a href="/w/index.php?title=Posebno:Prijava&amp;returnto=Dolenci%2C+%C5%A0alovci" title="Prijava ni obvezna, vendar je zaželena [o]" accesskey="o">Prijava</a></li>						</ul>
					</div>
									<div id="left-navigation">
										<div id="p-namespaces" role="navigation" class="vectorTabs" aria-labelledby="p-namespaces-label">
						<h3 id="p-namespaces-label">Imenski prostori</h3>
						<ul>
															<li  id="ca-nstab-main" class="selected"><span><a href="/wiki/Dolenci,_%C5%A0alovci"  title="Prikaže članek [c]" accesskey="c">Stran</a></span></li>
															<li  id="ca-talk" class="new"><span><a href="/w/index.php?title=Pogovor:Dolenci,_%C5%A0alovci&amp;action=edit&amp;redlink=1"  title="Pogovor o strani [t]" accesskey="t">Pogovor</a></span></li>
													</ul>
					</div>
										<div id="p-variants" role="navigation" class="vectorMenu emptyPortlet" aria-labelledby="p-variants-label">
												<h3 id="p-variants-label"><span>Različice</span><a href="#"></a></h3>

						<div class="menu">
							<ul>
															</ul>
						</div>
					</div>
									</div>
				<div id="right-navigation">
										<div id="p-views" role="navigation" class="vectorTabs" aria-labelledby="p-views-label">
						<h3 id="p-views-label">Pogled</h3>
						<ul>
															<li id="ca-view" class="selected"><span><a href="/wiki/Dolenci,_%C5%A0alovci" >Preberi</a></span></li>
															<li id="ca-ve-edit"><span><a href="/w/index.php?title=Dolenci,_%C5%A0alovci&amp;veaction=edit"  title="Urejanje strani z VisualEditorjem [v]" accesskey="v">Uredi</a></span></li>
															<li id="ca-edit" class=" collapsible"><span><a href="/w/index.php?title=Dolenci,_%C5%A0alovci&amp;action=edit"  title="Stran lahko uredite. Preden jo shranite, uporabite gumb za predogled. [e]" accesskey="e">Uredi kodo</a></span></li>
															<li id="ca-history" class="collapsible"><span><a href="/w/index.php?title=Dolenci,_%C5%A0alovci&amp;action=history"  title="Prejšnje redakcije strani [h]" accesskey="h">Zgodovina</a></span></li>
													</ul>
					</div>
										<div id="p-cactions" role="navigation" class="vectorMenu emptyPortlet" aria-labelledby="p-cactions-label">
						<h3 id="p-cactions-label"><span>Več</span><a href="#"></a></h3>

						<div class="menu">
							<ul>
															</ul>
						</div>
					</div>
										<div id="p-search" role="search">
						<h3>
							<label for="searchInput">Iskanje</label>
						</h3>

						<form action="/w/index.php" id="searchform">
														<div id="simpleSearch">
															<input type="search" name="search" placeholder="Iskanje" title="Preiščite wiki [f]" accesskey="f" id="searchInput" /><input type="hidden" value="Posebno:Iskanje" name="title" /><input type="submit" name="fulltext" value="Iskanje" title="Najde vneseno besedilo po straneh" id="mw-searchButton" class="searchButton mw-fallbackSearchButton" /><input type="submit" name="go" value="Pojdi na" title="Pojdi na stran z natanko takim imenom, če obstaja" id="searchButton" class="searchButton" />								</div>
						</form>
					</div>
									</div>
			</div>
			<div id="mw-panel">
				<div id="p-logo" role="banner"><a class="mw-wiki-logo" href="/wiki/Glavna_stran"  title="Glavna stran"></a></div>
						<div class="portal" role="navigation" id='p-navigation' aria-labelledby='p-navigation-label'>
			<h3 id='p-navigation-label'>Navigacija</h3>

			<div class="body">
									<ul>
													<li id="n-mainpage"><a href="/wiki/Glavna_stran" title="Obiščite Glavno stran [z]" accesskey="z">Glavna stran</a></li>
													<li id="n-Dobrodo.C5.A1li"><a href="/wiki/Wikipedija:Uvod">Dobrodošli</a></li>
													<li id="n-Izbrani-.C4.8Dlanki"><a href="/wiki/Wikipedija:Izbrani_%C4%8Dlanki">Izbrani članki</a></li>
													<li id="n-randompage"><a href="/wiki/Posebno:Naklju%C4%8Dno" title="Naložite naključno stran [x]" accesskey="x">Naključni članek</a></li>
													<li id="n-recentchanges"><a href="/wiki/Posebno:ZadnjeSpremembe" title="Seznam zadnjih sprememb Wikipedije [r]" accesskey="r">Zadnje spremembe</a></li>
											</ul>
							</div>
		</div>
			<div class="portal" role="navigation" id='p-obcestvo' aria-labelledby='p-obcestvo-label'>
			<h3 id='p-obcestvo-label'>Občestvo</h3>

			<div class="body">
									<ul>
													<li id="n-portal"><a href="/wiki/Wikipedija:Portal_ob%C4%8Destva" title="O projektu, kaj lahko storite, kje lahko kaj najdete">Portal občestva</a></li>
													<li id="n-Pod-lipo"><a href="/wiki/Wikipedija:Pod_lipo">Pod lipo</a></li>
													<li id="n-contact"><a href="/wiki/Wikipedija:Stik_z_nami">Kontaktna stran</a></li>
											</ul>
							</div>
		</div>
			<div class="portal" role="navigation" id='p-podpora' aria-labelledby='p-podpora-label'>
			<h3 id='p-podpora-label'>Podpora</h3>

			<div class="body">
									<ul>
													<li id="n-help"><a href="https://www.mediawiki.org/wiki/Special:MyLanguage/Help:Contents" title="Kraj za pomoč">Pomoč</a></li>
													<li id="n-sitesupport"><a href="//donate.wikimedia.org/wiki/Special:FundraiserRedirector?utm_source=donate&amp;utm_medium=sidebar&amp;utm_campaign=C13_sl.wikipedia.org&amp;uselang=sl" title="Podprite nas">Denarni prispevki</a></li>
											</ul>
							</div>
		</div>
			<div class="portal" role="navigation" id='p-coll-print_export' aria-labelledby='p-coll-print_export-label'>
			<h3 id='p-coll-print_export-label'>Tiskanje/izvoz</h3>

			<div class="body">
									<ul>
													<li id="coll-create_a_book"><a href="/w/index.php?title=Posebno:Book&amp;bookcmd=book_creator&amp;referer=Dolenci%2C+%C5%A0alovci">Ustvari e-knjigo</a></li>
													<li id="coll-download-as-rdf2latex"><a href="/w/index.php?title=Posebno:Book&amp;bookcmd=render_article&amp;arttitle=Dolenci%2C+%C5%A0alovci&amp;oldid=4145535&amp;writer=rdf2latex">Prenesi kot PDF</a></li>
													<li id="t-print"><a href="/w/index.php?title=Dolenci,_%C5%A0alovci&amp;printable=yes" title="Natisljiva različica strani [p]" accesskey="p">Različica za tisk</a></li>
											</ul>
							</div>
		</div>
			<div class="portal" role="navigation" id='p-tb' aria-labelledby='p-tb-label'>
			<h3 id='p-tb-label'>Orodja</h3>

			<div class="body">
									<ul>
													<li id="t-whatlinkshere"><a href="/wiki/Posebno:KajSePovezujeSem/Dolenci,_%C5%A0alovci" title="Seznam vseh s trenutno povezanih strani [j]" accesskey="j">Kaj se povezuje sem</a></li>
													<li id="t-recentchangeslinked"><a href="/wiki/Posebno:RecentChangesLinked/Dolenci,_%C5%A0alovci" title="Zadnje spremembe na s trenutno povezanih straneh [k]" accesskey="k">Sorodne spremembe</a></li>
													<li id="t-specialpages"><a href="/wiki/Posebno:PosebneStrani" title="Preglejte seznam vseh posebnih strani [q]" accesskey="q">Posebne strani</a></li>
													<li id="t-permalink"><a href="/w/index.php?title=Dolenci,_%C5%A0alovci&amp;oldid=4145535" title="Stalna povezava na to različico strani">Trajna povezava</a></li>
													<li id="t-info"><a href="/w/index.php?title=Dolenci,_%C5%A0alovci&amp;action=info" title="Več informacij o strani">Podatki o strani</a></li>
													<li id="t-wikibase"><a href="//www.wikidata.org/wiki/Q680734" title="Povežite na ustrezni predmet v podatkovnem odložišču [g]" accesskey="g">Objekt Wikipodatki</a></li>
						<li id="t-cite"><a href="/w/index.php?title=Posebno:Navedi&amp;page=Dolenci%2C_%C5%A0alovci&amp;id=4145535" title="Informacije o tem, kako navajati to stran">Navedba članka</a></li>					</ul>
							</div>
		</div>
			<div class="portal" role="navigation" id='p-lang' aria-labelledby='p-lang-label'>
			<h3 id='p-lang-label'>V drugih jezikih</h3>

			<div class="body">
									<ul>
													<li class="interlanguage-link interwiki-en"><a href="//en.wikipedia.org/wiki/Dolenci,_%C5%A0alovci" title="Dolenci, Šalovci – angleščina" lang="en" hreflang="en">English</a></li>
													<li class="interlanguage-link interwiki-eo"><a href="//eo.wikipedia.org/wiki/Dolenci" title="Dolenci – esperanto" lang="eo" hreflang="eo">Esperanto</a></li>
													<li class="interlanguage-link interwiki-hu"><a href="//hu.wikipedia.org/wiki/Dol%C3%A1ny_(Szlov%C3%A9nia)" title="Dolány (Szlovénia) – madžarščina" lang="hu" hreflang="hu">Magyar</a></li>
													<li class="interlanguage-link interwiki-nl"><a href="//nl.wikipedia.org/wiki/Dolenci" title="Dolenci – nizozemščina" lang="nl" hreflang="nl">Nederlands</a></li>
													<li class="interlanguage-link interwiki-ro"><a href="//ro.wikipedia.org/wiki/Dolenci,_%C5%A0alovci" title="Dolenci, Šalovci – romunščina" lang="ro" hreflang="ro">Română</a></li>
													<li class="interlanguage-link interwiki-uk"><a href="//uk.wikipedia.org/wiki/%D0%94%D0%BE%D0%BB%D0%B5%D0%BD%D1%86%D1%96_(%D0%A8%D0%B0%D0%BB%D0%BE%D0%B2%D1%86%D1%96)" title="Доленці (Шаловці) – ukrajinščina" lang="uk" hreflang="uk">Українська</a></li>
													<li class="uls-p-lang-dummy"><a href="#"></a></li>
											</ul>
				<div class='after-portlet after-portlet-lang'><span class="wb-langlinks-edit wb-langlinks-link"><a href="//www.wikidata.org/wiki/Q680734#sitelinks-wikipedia" title="Uredi medjezikovne povezave" class="wbc-editpage">Uredi povezave</a></span></div>			</div>
		</div>
				</div>
		</div>
		<div id="footer" role="contentinfo">
							<ul id="footer-info">
											<li id="footer-info-lastmod"> Čas zadnje spremembe: 14:33, 14. januar 2014.</li>
											<li id="footer-info-copyright">Besedilo se sme prosto uporabljati v skladu z dovoljenjem <a href="http://creativecommons.org/licenses/by-sa/3.0/">Creative Commons 
Priznanje avtorstva-Deljenje pod enakimi pogoji 3.0</a>; uveljavljajo se lahko dodatni pogoji. Za podrobnosti glej <a href="//wikimediafoundation.org/wiki/Terms_of_Use">Pogoje uporabe</a>.<br/>
Wikipedia® je tržna znamka neprofitne organizacije <a href="http://wikimediafoundation.org">Wikimedia Foundation Inc.</a></li>
									</ul>
							<ul id="footer-places">
											<li id="footer-places-privacy"><a href="//wikimediafoundation.org/wiki/Politika_zasebnosti" title="wikimedia:Politika zasebnosti">Politika zasebnosti</a></li>
											<li id="footer-places-about"><a href="/wiki/Wikipedija:O_Wikipediji" title="Wikipedija:O Wikipediji">O Wikipediji</a></li>
											<li id="footer-places-disclaimer"><a href="/wiki/Wikipedija:Splo%C5%A1no_zanikanje_odgovornosti" title="Wikipedija:Splošno zanikanje odgovornosti">Zanikanja odgovornosti</a></li>
											<li id="footer-places-developers"><a href="https://www.mediawiki.org/wiki/Special:MyLanguage/How_to_contribute">Razvijalci</a></li>
											<li id="footer-places-mobileview"><a href="//sl.m.wikipedia.org/w/index.php?title=Dolenci,_%C5%A0alovci&amp;mobileaction=toggle_view_mobile" class="noprint stopMobileRedirectToggle">Mobilni pogled</a></li>
									</ul>
										<ul id="footer-icons" class="noprint">
											<li id="footer-copyrightico">
															<a href="//wikimediafoundation.org/"><img src="//bits.wikimedia.org/images/wikimedia-button.png" srcset="//bits.wikimedia.org/images/wikimedia-button-1.5x.png 1.5x, //bits.wikimedia.org/images/wikimedia-button-2x.png 2x" width="88" height="31" alt="Wikimedia Foundation"/></a>
													</li>
											<li id="footer-poweredbyico">
															<a href="//www.mediawiki.org/"><img src="//bits.wikimedia.org/static-1.25wmf22/resources/assets/poweredby_mediawiki_88x31.png" alt="Powered by MediaWiki" srcset="//bits.wikimedia.org/static-1.25wmf22/resources/assets/poweredby_mediawiki_132x47.png 1.5x, //bits.wikimedia.org/static-1.25wmf22/resources/assets/poweredby_mediawiki_176x62.png 2x" width="88" height="31" /></a>
													</li>
									</ul>
						<div style="clear:both"></div>
		</div>
		<script>/*<![CDATA[*/window.jQuery && jQuery.ready();/*]]>*/</script><script>var _mwq = _mwq || []; _mwq.push( function ( mw ) { mw.loader.state({"ext.globalCssJs.site":"ready","ext.globalCssJs.user":"ready","site":"loading","user":"ready","user.groups":"ready"}); } );</script>
<script>var _mwq = _mwq || []; _mwq.push( function ( mw ) { mw.loader.load(["mediawiki.action.view.postEdit","mediawiki.user","mediawiki.hidpi","mediawiki.page.ready","mediawiki.searchSuggest","ext.wikiEditor.init","mmv.bootstrap.autostart","ext.imageMetrics.loader","ext.visualEditor.targetLoader","ext.eventLogging.subscriber","ext.wikimediaEvents.statsd","ext.navigationTiming","schema.UniversalLanguageSelector","ext.uls.eventlogger","ext.uls.interlanguage"],null,true); } );</script>
<script>var _mwq = _mwq || []; _mwq.push( function ( mw ) { document.write("\u003Cscript src=\"//bits.wikimedia.org/sl.wikipedia.org/load.php?debug=false\u0026amp;lang=sl\u0026amp;modules=site\u0026amp;only=scripts\u0026amp;skin=vector\u0026amp;*\"\u003E\u003C/script\u003E"); } );</script>
<script>var _mwq = _mwq || []; _mwq.push( function ( mw ) { mw.config.set({"wgBackendResponseTime":95,"wgHostname":"mw1181"}); } );</script>
	</body>
</html>
